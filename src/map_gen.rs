
use noise::{NoiseFn, OpenSimplex, Seedable};

use engine::{
	entity::MapGenerator,
	entity::EntityStore,
	data::*,
};

use crate::template;


pub struct MapGen {
	noise: OpenSimplex,
	trixels_per_hexture_edge: u8,
	floor_size: u8,
}

impl MapGen {
	pub fn new(trixels_per_hexture_edge: u8, floor_size: u8) -> Self {
		
		let seed = rand::random();
		println!("Using seed: {:X}", seed);
		
		let noise = OpenSimplex::new().set_seed(seed);
		
		
		Self {
			noise,
			trixels_per_hexture_edge,
			floor_size,
		}
	}
}


impl MapGenerator for MapGen {
	fn start_tile(&self, position: Position, entities: &mut EntityStore) -> Player {
		
		template::ground::new(entities, position, &self.noise, self.trixels_per_hexture_edge)
			.create();
		
		let player_id = entities.new_entity()
			.has(position)
			.create();
		
		Player::new(player_id)
	}
	
	
	fn tile(&self, position: Position, entities: &mut EntityStore) {
		
		if Position::new(0, 0).to(position).distance_b_line() > self.floor_size-1 {
			entities.new_entity()
				.has(position)
				.has(Impassable)
				.create();
		}
		else {
			let pos_noise = self.noise.get([position.col as f64, position.row as f64]);
			
			
			if pos_noise > 0.45 {
				template::water::new(entities, position, &self.noise, self.trixels_per_hexture_edge)
					.create();
			}
			else {
				if pos_noise > 0.4 {
					template::pebble::new(entities, position, &self.noise, self.trixels_per_hexture_edge)
						.create();
				}
				else if pos_noise > 0.1 {
					template::grass_blade::new(entities, position, &self.noise)
						.create();
				}
				else if pos_noise > 0.09 {
					let direction = {
						let direction_noise = (pos_noise.abs() * 10000.0) as usize % 6;
						
						Direction::variants()[direction_noise]
					};
					
					template::ant::new(entities)
						.has(position)
						.has(direction)
						.create();
				}
				
				
				template::ground::new(entities, position, &self.noise, self.trixels_per_hexture_edge)
					.create();
			}
		}
	}
}
