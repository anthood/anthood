use std::collections::HashSet;


use engine::{
	bind,
	input::{
		keyboard::KeyCode,
		keyboard::KeyCode::*,
		input_action::*,
		key_binding::KeyBinding,
	},
	data::Direction::*,
};



pub fn key_bindings() -> Vec<KeyBinding> {
	vec![
		bind![Move(North)	 => W],
		bind![Move(NorthEast) => E],
		bind![Move(SouthEast) => D],
		bind![Move(South)	 => S],
		bind![Move(SouthWest) => A],
		bind![Move(NorthWest) => Q],
		
		bind![Turn(North)	 => LShift, W],
		bind![Turn(NorthEast) => LShift, E],
		bind![Turn(SouthEast) => LShift, D],
		bind![Turn(South)	 => LShift, S],
		bind![Turn(SouthWest) => LShift, A],
		bind![Turn(NorthWest) => LShift, Q],
		
		bind![ZoomIn  => Plus],
		bind![ZoomOut => Minus],
		bind![ToggleFullscreen => F11],
	]
}
