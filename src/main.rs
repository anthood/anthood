
use engine::{
	GameResult,
	setup::Engine,
	setup::HextureDimension,
};

mod controls;

mod data;

mod template;

mod map_gen;
use map_gen::MapGen;

mod util;


fn main() -> GameResult {
	
	let trixels_per_hexture_edge = 24;
	let floor_size = 20;
	let map_generator = MapGen::new(trixels_per_hexture_edge, floor_size);
	let hexture_dimension = HextureDimension::new(32.0, trixels_per_hexture_edge, 0);
	let draw_range = 30;
	
	
	Engine::start(
		"Anthood",
		&map_generator,
		hexture_dimension,
		floor_size,
		draw_range,
		controls::key_bindings(),
	)
}
