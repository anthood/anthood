use super::*;

pub fn new<'a>(entities: &'a mut EntityStore, position: Position, noise: &OpenSimplex) -> EntityBuilder<'a> {
	entities.new_entity()
		.has(GrassBlade)
		.has(Height(120.0))
		.has(Impassable)
		.has(Health::full(12))
		.has(position)
		.has(hexture(&position, noise))
}


fn hexture(position: &Position, noise: &OpenSimplex) -> Hexture {
	
	let mut hexture = Hexture::new();
	
	hexture.hexagon(
		trixel::Position::new(0, 0),
		12,
		&|_| { Color::new(0.0, 0.05, 0.0, 1.0) },
	);
	
	hexture.hexagon(
		trixel::Position::new(0, 0),
		11,
		&|trixel_pos| {
			let normalized_trixel_row = if trixel_pos.y > 0 {trixel_pos.y+1} else {trixel_pos.y};
			
			let noise_value = noise.get([
				position.col as f64,
				position.row as f64,
				(trixel_pos.x.abs()/2) as f64,
				(normalized_trixel_row.abs()/2) as f64,
			]).abs() as f32;
			
			
			Color::new(
				0.0,
				(noise_value * 10.0 % 0.7).max(0.1),
				noise_value * 10.0 % 0.1,
				1.0,
			)
		}
	);
	
	hexture
}
