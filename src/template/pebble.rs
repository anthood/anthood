use super::*;

pub fn new<'a>(entities: &'a mut EntityStore, position: Position, noise: &OpenSimplex, trixels_per_hexture_edge: u8) -> EntityBuilder<'a> {
	entities.new_entity()
		.has(position)
		.has(Height(8.0))
		.has(Impassable)
		.has(hexture(&position, noise, trixels_per_hexture_edge))
}

fn hexture(position: &Position, noise: &OpenSimplex, trixels_per_hexture_edge: u8) -> Hexture {
	
	let center = trixel::Position::new(0, 0);
	let mut hexture = Hexture::new();
	
	hexture.hexagon(
		center,
		trixels_per_hexture_edge - 8,
		&|_| { Color::new(0.3, 0.3, 0.3, 1.0) }
	);
	
	hexture.hexagon(
		center,
		trixels_per_hexture_edge - 9,
		&|_| {
			let noise_value = noise.get([
				position.col as f64,
				position.row as f64,
			]) as f32;
			
			
			Color::new(
				((1.0 + noise_value) % 0.3).max(0.2) * 2.0,
				((1.0 + noise_value) % 0.3).max(0.2) * 2.0,
				((1.0 + noise_value) % 0.3).max(0.2) * 2.0,
				1.0,
			)
		}
	);
	
	hexture
}
