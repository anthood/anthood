use noise::{NoiseFn, OpenSimplex};

use engine::{
	action::Behavior,
	entity::{EntityStore, EntityBuilder},
	graphics::color::*,
	data::*,
};

use crate::data::*;


pub mod ant;

pub mod ground;

pub mod water;

pub mod grass_blade;

pub mod pebble;
