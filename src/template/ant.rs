use super::*;

mod hexture;
mod behavior;


pub fn new(entities: &mut EntityStore) -> EntityBuilder {
	entities.new_entity()
		.has(Height(3.0))
		.has(Impassable)
		.has(Behavior::new(behavior::take_turn))
		.has(hexture::hexture())
}
