use engine::{
	action::*,
	data::*,
	entity::{EntityId, EntityStore},
};

use crate::data::*;


pub fn take_turn(self_id: EntityId, entities: &mut EntityStore) {
	let vision_range = 8;
	
	if let Some(&current_position) = entities.get::<Position>(self_id) {
		
		let nearest_grass_blade = find_closest_matching_entity(
			|id, entities| entities.get::<GrassBlade>(id).is_some(),
			current_position,
			vision_range,
			entities,
		);
		
		if let Some(nearest_grass_blade) = nearest_grass_blade {
			let moved = move_towards_position(self_id, current_position, nearest_grass_blade, vision_range, entities);
			
			if moved == false {
				move_into_random_direction(self_id, entities);
			}
		}
		else {
			move_into_random_direction(self_id, entities);
		}
	}
}


fn find_closest_matching_entity(matcher: fn(EntityId, &EntityStore) -> bool, current_position: Position, range: u8, entities: &mut EntityStore) -> Option<Position> {
	HexagonalArea::new(current_position, range)
		.spiral_iter()
		.find(|position| {
			entities.at(position).iter()
				.any(|id| matcher(*id, entities))
		})
}


fn move_towards_position(self_id: EntityId, current_position: Position, target_position: Position, max_range: u8, entities: &mut EntityStore) -> bool {
	
	let distance = current_position.to(target_position);
	
	crate::util::calculate_shortest_path(distance, max_range, entities).map(|path| {
		path.get(0).cloned().map(|mut first_step_direction| {
			
			if let Some(current_direction) = entities.get::<Direction>(self_id) {
				if first_step_direction == current_direction.opposite() {
					first_step_direction = current_direction.neighbor_cw()
				}
			}
			
			let step_towards = StepTowards::new(self_id, first_step_direction);
			
			let result = step_towards.apply(entities);
			
			if result.is_err() {
				log::debug!("Action {:?} was chosen, but could not be executed.", step_towards);
			}
		})
	}).flatten().is_some()
}


fn move_into_random_direction(self_id: EntityId, entities: &mut EntityStore) {
	use rand::thread_rng;
	use rand::seq::SliceRandom;
	
	if let Some(&current_direction) = entities.get::<Direction>(self_id) {
		
		let possible_directions = [
			current_direction.neighbor_ccw(),
			current_direction,
			current_direction, //double chance to continue moving into same direction
			current_direction.neighbor_cw()
		];
		
		let mut rng = thread_rng();
		
		if let Some(&new_direction) = possible_directions.choose(&mut rng) {
			
			let step_towards = StepTowards::new(self_id, new_direction);
			
			let result = step_towards.apply(entities);
			
			if result.is_err() {
				log::debug!("Action {:?} was chosen, but could not be executed.", step_towards);
			}
		}
	}
}
