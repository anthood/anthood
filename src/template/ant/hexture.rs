use super::*;


pub(super) fn hexture() -> Hexture {
	use trixel::Direction::*;
		
		let mut hexture = Hexture::new();
		
		let color = Color::new(0.05, 0.01, 0.0, 1.0);
		
		let pos = |x, y| {
			trixel::Position::new(x, y)
		};
		
		
		let mut antennae = |origin| {
			hexture.line(origin, Deg30, 6, color);
			hexture.line(origin + pos(3, -3), Deg330, 8, color);
		};
		antennae(pos(5, -6));
		
		
		let mut mandibles = |origin| {
			hexture.line(origin, Deg330, 3, color);
		};
		mandibles(pos(4, -7));
		
		
		let mut head = |origin| {
			hexture.hexagon(origin, 3, &|_| {color});
		};
		head(pos(0, -4));
		
		
		let mut fore_leg = |origin| {
			hexture.line(origin, Deg30, 10, color);
		};
		fore_leg(pos(4,  0));
		
		
		let mut alitrunk = |origin| {
			hexture.hexagon(origin, 2, &|_| {color});
			hexture.hexagon(origin + pos(0, 2), 2, &|_| {color});
		};
		alitrunk(pos(0, 0));
		
		
		let mut middle_leg = |origin| {
			hexture.line(origin, Deg90, 3, color);
			hexture.line(origin + pos(3, 0), Deg150, 8, color);
		};
		middle_leg(pos(4, 2));
		
		
		let mut hind_leg = |origin| {
			hexture.line(origin, Deg90, 2, color);
			hexture.line(origin + pos(2, 0), Deg150, 9, color);
		};
		hind_leg(pos(4, 4));
		
		
		let mut petiole = |origin| {
			hexture.hexagon(origin, 2, &|_| {color});
		};
		petiole(pos(0, 4));
		
		
		let mut gaster = |origin| {
			hexture.hexagon(origin, 3, &|_| {color});
			hexture.hexagon(origin + pos(0, 2), 3, &|_| {color});
		};
		gaster(pos(0, 8));
		
		
		
		let mirrored = hexture.clone().mirrored_horizontally();
		
		hexture.stamped_with(&mirrored)
}
