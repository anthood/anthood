
use std::{
	collections::VecDeque,
	collections::BTreeMap,
	collections::btree_map::Entry,
};

use engine::{
	data::Impassable,
	data::Direction,
	data::Distance,
	entity::EntityStore,
};


pub fn calculate_shortest_path(distance: Distance, max_range: u8, entities: &EntityStore) -> Option<Vec<Direction>> {
	
	//breadth-first search
	let mut came_from = BTreeMap::new();
	let mut frontier = VecDeque::new();
	frontier.push_back(distance.start);
	
	
	while let Some(current) = frontier.pop_front() {
			if current == distance.end {
					break;
			}
			
			for direction in Direction::variants().iter() {
					let neighbor = current + direction.normalized_vector();
					
					if distance.start.to(neighbor).distance_b_line() > max_range {
							continue;
					}
					
					if let Entry::Vacant(came_to_neighbor_from) = came_from.entry(neighbor) {
							let neighbor_is_impassable =
									entities.at(&neighbor).iter()
											.any(|id| entities.get::<Impassable>(*id).is_some());
							
							if neighbor == distance.end //allow path-finding towards a position which is itself impassable (e.g. a creature)
							|| !neighbor_is_impassable {
									frontier.push_back(neighbor);
									came_to_neighbor_from.insert(direction.opposite());
							}
					}
			}
	}
	
	//Construct path by walking from the goal backwards
	let mut current = distance.end;
	let mut path = vec![];
	while current != distance.start {
			path.push(came_from.get(&current)?.opposite());
			current = current + came_from.get(&current)?.normalized_vector();
	}
	
	path.reverse();
	Some(path)
}


#[cfg(test)]
mod tests {
	use super::*;
	
	#[test]
	fn calculate_shortest_path() {
		use Direction::*;
		
		let max_range = 10;
		let mut entities = EntityStore::new();
		
		fn block(position: Position, entities: &mut EntityStore) {
			entities.new_entity()
				.has(Impassable)
				.has(position)
				.create();
		}
		
		
		let south = Position::new(0,  1);
		let north = Position::new(0, -1);
		
		
		//direct path
		assert_eq!(
			south.to(north).calculate_shortest_path(max_range, &entities),
			Some(vec![North, North])
		);
		
		
		
		//obstacle in the direct path
		block(Position::new(0, 0), &mut entities);
		let path = south.to(north).calculate_shortest_path(max_range, &entities);
		assert!(
			path == Some(vec![NorthEast, North, NorthWest])
		 || path == Some(vec![NorthWest, North, NorthEast])
		);
		
		
		
		block(Position::new( 1, 0), &mut entities);
		block(Position::new( 1, 1), &mut entities);
		block(Position::new( 0, 2), &mut entities);
		block(Position::new(-1, 2), &mut entities);
		block(Position::new(-1, 1), &mut entities);
		
		//starting position completely surrounded by obstacles
		assert_eq!(
			south.to(north).calculate_shortest_path(max_range, &entities),
			None
		);
		
		//target position completely surrounded by obstacles
		assert_eq!(
			north.to(south).calculate_shortest_path(max_range, &entities),
			None
		);
	}
}
