
use crate::data::{Position, Player};

use super::EntityStore;


pub trait MapGenerator {
	fn start_tile(&self, position: Position, entities: &mut EntityStore) -> Player;
	
	fn tile(&self, position: Position, entities: &mut EntityStore);
}
