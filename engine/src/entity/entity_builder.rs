use std::collections::BTreeMap;
use std::any::{Any, TypeId};

use super::{EntityId, EntityStore};


#[must_use]
pub struct EntityBuilder<'a> {
	id: EntityId,
	entity_store: &'a mut EntityStore,
	components: BTreeMap<TypeId, Box<dyn Any + 'static>>,
}

impl<'a> EntityBuilder<'a> {
	pub fn new(id: EntityId, entity_store: &'a mut EntityStore) -> Self {
		Self {
			id,
			entity_store,
			components: BTreeMap::new(),
		}
	}
	
	
	pub fn has<T: 'static>(mut self, component: T) -> EntityBuilder<'a> {
		self.components
			.insert(TypeId::of::<T>(), Box::new(component));
		
		self
	}
	
	
	pub fn create(self) -> EntityId {
		for (type_id, component) in self.components {
			self.entity_store.put_raw(self.id, type_id, component);
		}
		
		self.id
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	
	#[derive(Clone, Copy, Debug, PartialEq)]
	struct Data(pub u8);

	#[test]
	fn create_entity() {
		let id = EntityId(0);
		let mut entity_store = EntityStore::new();
		let builder = EntityBuilder::new(id, &mut entity_store);
		
		let data = Data(1);
		
		builder
			.has(data)
			.create();
		
		let result = entity_store.get::<Data>(id);
		
		assert_eq!(result, Some(&data));
	}
}
