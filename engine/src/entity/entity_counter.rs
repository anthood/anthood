use std::sync::atomic::{AtomicU64, Ordering};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct EntityId(pub u64);


#[derive(Default)]
pub struct EntityCounter {
	last_used_id: AtomicU64,
}

impl EntityCounter {
	pub fn new() -> Self {
		Self { last_used_id: AtomicU64::new(0) }
	}
	
	pub fn get_next_id(&self) -> EntityId {
		EntityId(self.last_used_id.fetch_add(1, Ordering::SeqCst))
	}
}


#[cfg(test)]
mod tests {
	use super::EntityCounter;
	
	#[test]
	fn get_next_id() {
		let entity_counter = EntityCounter::new();
		
		let id = entity_counter.get_next_id().0;
		
		assert_eq!(entity_counter.get_next_id().0, id + 1);
		assert_eq!(entity_counter.get_next_id().0, id + 2);
		assert_eq!(entity_counter.get_next_id().0, id + 3);
	}
}
