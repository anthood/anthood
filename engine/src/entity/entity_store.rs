use std::collections::{BTreeMap, btree_map::Entry, BTreeSet};
use std::any::{Any, TypeId};

use crate::data::Position;

use super::entity_counter::EntityCounter;
pub use super::entity_counter::EntityId;

use super::entity_builder::EntityBuilder;


#[derive(Default)]
pub struct EntityStore {
	entity_counter: EntityCounter,
	data: BTreeMap<TypeId, BTreeMap<EntityId, Box<dyn Any + 'static>>>,
	position_to_entities: BTreeMap<Position, BTreeSet<EntityId>>,
	empty: BTreeSet<EntityId>,
}


impl EntityStore {
	pub fn new() -> Self {
		Self {
			entity_counter: EntityCounter::new(),
			data: BTreeMap::new(),
			position_to_entities: BTreeMap::new(),
			empty: BTreeSet::new(),
		}
	}
	
	
	pub fn new_entity(&mut self) -> EntityBuilder {
		EntityBuilder::new(self.entity_counter.get_next_id(), self)
	}
	
	
	pub fn get<T: 'static>(&self, id: EntityId) -> Option<&T> {
		self.data
			.get(&TypeId::of::<T>())
			.map(|component_map|
				component_map
					.get(&id)
					.map(|component_of_entity|
						component_of_entity.downcast_ref::<T>().unwrap()
					)
			)
			.flatten()
	}
	
	
	pub fn with<T: 'static>(&self) -> BTreeMap<EntityId, &T> {
		match self.data.get(&TypeId::of::<T>()) {
			Some(component_map) => {
				component_map
					.iter()
					.map(|component_of_entity| {
						let (id, value) = component_of_entity;
						(*id, value.downcast_ref::<T>().unwrap())
					})
					.collect()
			}
			None => BTreeMap::new()
		}
	}
	
	
	pub fn at(&self, position: &Position) -> &BTreeSet<EntityId> {
		self.position_to_entities
			.get(&position)
			.unwrap_or(&self.empty)
	}
	
	
	pub(super) fn put_raw(&mut self, id: EntityId, type_id: TypeId, component: Box<dyn Any>) {
		
		//update pos_to_entities
		if let Some(new_position) = component.downcast_ref::<Position>() {
			if let Some(old_position) = self.get::<Position>(id).cloned() {
				if let Entry::Occupied(mut entities) = self.position_to_entities.entry(old_position) {
					entities.get_mut().remove(&id);
				}
			}
			
			self.position_to_entities
				.entry(*new_position).or_insert_with(BTreeSet::new)
				.insert(id);
		}
		
		//update actual data
		self.data
			.entry(type_id).or_insert_with(BTreeMap::new)
			.insert(id, component);
	}
	
	
	pub fn put<T: 'static>(&mut self, id: EntityId, component: T) {
		self.put_raw(id, TypeId::of::<T>(), Box::new(component));
	}
	
	
	pub fn remove<T: 'static>(&mut self, id: EntityId) {
		
		if let Entry::Occupied(mut entities) = self.data.entry(TypeId::of::<T>()) {
			entities.get_mut().remove(&id);
		}
	}
	
	
	pub fn delete_entity(&mut self, id: EntityId) {
		if let Some(position) = self.get::<Position>(id).cloned() {
			if let Entry::Occupied(mut entities_at_position) = self.position_to_entities.entry(position) {
				entities_at_position.get_mut().remove(&id);
			}
		}
		
		self.data.values_mut().for_each(|component_storage| {
			component_storage.remove(&id);
		});
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	
	
	#[derive(Clone, Copy, Debug, PartialEq)]
	struct Data(pub u8);
	
	
	#[test]
	fn get_empty_component() {
		let entities = EntityStore::new();
		
		let id = EntityId(0);
		
		let result = entities.get::<Data>(id);
		
		assert_eq!(result, None);
	}
	
	
	#[test]
	fn put_and_get() {
		let mut entities = EntityStore::new();
		
		let id = EntityId(0);
		
		let data = Data(0);
		entities.put(id, data);
		
		let result = entities.get::<Data>(id);
		
		assert_eq!(result, Some(&data));
	}
	
	
	#[test]
	fn put_update_and_get() {
		let mut entities = EntityStore::new();
		
		let id = EntityId(0);
		
		let data0 = Data(0);
		entities.put(id, data0);
		
		let data1 = Data(2);
		entities.put(id, data1);
		
		let result = entities.get::<Data>(id);
		
		assert_eq!(result, Some(&data1));
	}
	
	
	#[test]
	fn put_and_with() {
		let mut entities = EntityStore::new();
		
		let id0 = EntityId(0);
		let data0 = Data(0);
		entities.put(id0, data0);
		
		let id1 = EntityId(1);
		let data1 = Data(1);
		entities.put(id1, data1);
		
		let result = entities.with::<Data>();
		
		assert!(result.contains_key(&id0));
		assert_eq!(result.get(&id0), Some(&&data0));
		
		assert!(result.contains_key(&id1));
		assert_eq!(result.get(&id1), Some(&&data1));
	}
	
	
	#[test]
	fn put_and_at() {
		let mut entities = EntityStore::new();
		
		let id0 = EntityId(0);
		let position0 = Position::new(1, 2);
		entities.put(id0, position0);
		
		let id1 = EntityId(1);
		entities.put(id1, position0);
		
		let id2 = EntityId(2);
		let position1 = Position::new(3, 4);
		entities.put(id2, position1);
		
		let result = entities.at(&position0);
		assert!(result.contains(&id0));
		assert!(result.contains(&id1));
		assert!(!result.contains(&id2));
		
		entities.put(id1, position1); //move entity to new pos
		
		let result = entities.at(&position0);
		assert!(result.contains(&id0));
		assert!(!result.contains(&id1));
		assert!(!result.contains(&id2));
		
		let result = entities.at(&position1);
		assert!(!result.contains(&id0));
		assert!(result.contains(&id1));
		assert!(result.contains(&id2));
		
		let empty_position = Position::new(5, 6);
		let result = entities.at(&empty_position);
		assert_eq!(result, &BTreeSet::new());
	}
	
	
	#[test]
	fn put_and_remove() {
		let mut entities = EntityStore::new();
		
		let id = EntityId(0);
		
		let data = Data(0);
		entities.put(id, data);
		
		assert_eq!(entities.get::<Data>(id), Some(&data));
		
		entities.remove::<Data>(id);
		
		assert_eq!(entities.get::<Data>(id), None);
	}
	
	
	#[test]
	fn put_and_delete_entity() {
		let mut entities = EntityStore::new();
		
		let id = EntityId(0);
		
		let data = Data(0);
		entities.put(id, data);
		let position = Position::new(1, 2);
		entities.put(id, position);
		
		assert_eq!(entities.get::<Data>(id), Some(&data));
		assert!(entities.at(&position).contains(&id));
		
		entities.delete_entity(id);
		
		assert_eq!(entities.get::<Data>(id), None);
		assert!(!entities.at(&position).contains(&id));
	}
}
