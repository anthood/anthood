
type HealthValue = u16;

#[derive(Clone, Debug)]
pub struct Health {
	pub current_health: HealthValue,
	pub max_health: HealthValue,
}

impl Health {
	pub fn full(max_health: HealthValue) -> Self {
		Self { current_health: max_health, max_health }
	}
}
