use ggez::graphics::Mesh;

use std::collections::BTreeMap;

use crate::graphics::{
	color::*,
	unit::*,
	render,
};

use super::trixel;


/// Information for rendering a hexagonal texture. Consists out of triangles which serve like pixels in pixel graphics (here referred to as 'trixels').
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Hexture { pub trixels: BTreeMap<trixel::Position, Color> }
impl Hexture {
	pub fn new() -> Self {
		Self { trixels: BTreeMap::new() }
	}
	
	
	///Sets an individual trixel to the given `color`.
	pub fn set(&mut self, pos: trixel::Position, color: Color) -> &Self {
		self.trixels.insert(pos, color);
		
		self
	}
	
	
	///Draws a line from the `start` into the given `direction` with the given `length`.
	pub fn line(&mut self, start: trixel::Position, direction: trixel::Direction, length: u8, color: Color) -> &Self {
		
		if length == 0 {
			self
		}
		else {
			self.set(start, color);
			self.line(start.position_in_direction(direction), direction, length - 1, color)
		}
	}
	
	
	///Stamps a hexagon-shape at the given `center` with the given `size`.
	///
	///Mind that due to the triangle grid, this will only look good when `center.points_north()` is true.
	pub fn hexagon(&mut self, center: trixel::Position, size: u8, color: &dyn Fn(trixel::Position) -> Color) -> &Self {
		
		let size = trixel::Coord::from(size);
		
		for row in 1..=size {
			let col_size = size*2 - row;
			for col in -col_size..=col_size {
				let pos = trixel::Position::new(col, -row) + center;
				self.set(pos, color(pos));
				let pos = trixel::Position::new(col, row-1) + center;
				self.set(pos, color(pos)); //count row from 0
			}
		}
		
		self
	}
	
	
	pub fn stamped_with(mut self, other: &Hexture) -> Self {
		for (pos, color) in other.clone().trixels.iter() {
			self.set(*pos, *color);
		}
		
		self
	}
	
	
	pub fn mirrored_horizontally(self) -> Self {
		let mut mirrored = Hexture::new();
		
		for (trixel::Position { x, y }, color) in self.trixels.iter() {
			mirrored.set(trixel::Position::new(-x, *y), *color);
		}
		
		mirrored
	}
	
	
	pub fn render(&self, trixel_size: PixelVector, context: &mut ggez::Context) -> Mesh {
		render::hexture(self, trixel_size, context)
	}
}


#[cfg(test)]
mod tests {
	use super::*;
	
	#[test]
	fn stamped_with() {
		let mut expectation = Hexture::new();
		
		let mut original = Hexture::new();
		let original_trixel_pos = trixel::Position::new(-1, -2);
		original.set(original_trixel_pos, Color::RED);
		expectation.set(original_trixel_pos, Color::RED);
		
		let mut stamp = Hexture::new();
		let stamp_trixel_pos = trixel::Position::new(3, 4);
		stamp.set(stamp_trixel_pos, Color::BLUE);
		expectation.set(stamp_trixel_pos, Color::BLUE);
		
		assert_ne!(original, expectation);
		assert_ne!(stamp, expectation);
		
		assert_eq!(original.stamped_with(&stamp), expectation);
	}
	
	#[test]
	fn mirror_horizontally() {
		let mut hexture = Hexture::new();
		hexture.set(trixel::Position::new(1, -2), Color::GREEN);
		
		assert_ne!(hexture, hexture.clone().mirrored_horizontally());
		
		assert_eq!(hexture, hexture.clone().mirrored_horizontally().mirrored_horizontally());
	}
}
