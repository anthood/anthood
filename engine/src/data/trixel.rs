use crate::graphics::unit::*;


#[derive(Clone, Copy)]
pub enum Direction {
	  Deg0,  Deg30,  Deg60,
	 Deg90, Deg120, Deg150,
	Deg180, Deg210, Deg240,
	Deg270, Deg300, Deg330,
}


pub type Coord = i16;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Position {
	pub x: Coord,
	pub y: Coord,
}

impl Position {
	pub fn new(x: Coord, y: Coord) -> Self {
		Self { x, y }
	}
	
	
	pub fn pixel_within_hexture(&self, trixel_size: PixelVector) -> PixelPoint {
		PixelPoint::new(
			f32::from(self.x) * trixel_size.x / 2.0,
			f32::from(self.y) * trixel_size.y
			+ (trixel_size.y / 2.0) //ensure the actual center of the hexture is the center, not (0,0)
		)
	}
	
	
	pub fn position_in_direction(&self, direction: Direction) -> Self {
		use Direction::*;
		
		
		let (x, y) = if self.points_north() {
			match direction {
				Deg0   => ( 0, -1),
				Deg90  => ( 1,  0),
				Deg180 => ( 0,  1),
				Deg270 => (-1,  0),
				
				Deg30  => ( 1,  0),
				Deg150 => ( 0,  1),
				Deg210 => ( 0,  1),
				Deg330 => (-1,  0),
				
				Deg60  => ( 1,  0),
				Deg120 => ( 2,  1),
				Deg240 => (-2,  1),
				Deg300 => (-1,  0),
			}
		} else {
			match direction {
				Deg0   => ( 0, -1),
				Deg90  => ( 1,  0),
				Deg180 => ( 0,  1),
				Deg270 => (-1,  0),
				
				Deg30  => ( 0, -1),
				Deg150 => ( 1,  0),
				Deg210 => (-1,  0),
				Deg330 => ( 0, -1),
				
				Deg60  => ( 2, -1),
				Deg120 => ( 1,  0),
				Deg240 => (-1,  0),
				Deg300 => (-2, -1),
			}
		};
		
		Self {
			x: self.x + x,
			y: self.y + y
		}
	}
	
	
	/// Returns whether a particular trixel points north, as opposed to south (other orientations are not possible).
	pub fn points_north(&self) -> bool {
		(self.x + self.y) % 2 == 0
	}
}


impl std::ops::Add<Self> for Position {
	type Output = Self;
	
	fn add(self, other: Self) -> Self {
		Self {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}



#[cfg(test)]
mod tests {
	use super::*;
	use super::super::trixel::Position;
	use crate::graphics::*;
	
	#[test]
	fn pixel_within_hexture() {
		let trixel_size: PixelVector = HextureDimension::new(12.0, 2, 0).trixel_size();
		
		assert_eq!(
			Position::new(0, 0).pixel_within_hexture(trixel_size),
			PixelPoint::new(0.0, trixel_size.y / 2.0)
		);
		
		assert_eq!(
			Position::new(1, 0).pixel_within_hexture(trixel_size),
			PixelPoint::new(trixel_size.x / 2.0, trixel_size.y / 2.0)
		);
		
		assert_eq!(
			Position::new(0, 1).pixel_within_hexture(trixel_size),
			PixelPoint::new(0.0, 3.0 * trixel_size.y / 2.0)
		);
	}
}
