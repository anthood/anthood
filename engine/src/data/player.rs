use super::EntityId;

#[derive(Clone, Copy)]
pub struct Player { pub id: EntityId }

impl Player {
	pub fn new(id: EntityId) -> Self {
		Self { id }
	}
}
