
pub use crate::graphics::HextureDimension;

use ggez::{Context, graphics};


use crate::{
	action::Behavior,
	data::{Position, Player, HexagonalArea, Hexture},
	input::InputHandler,
	input::key_binding::KeyBinding,
	input::keyboard::{KeyCode, KeyMods},
	graphics::*,
	graphics::color::*,
	entity::{EntityId, EntityStore, MapGenerator},
	GameResult,
};



pub struct Engine {
	entities: EntityStore,
	input_handler: InputHandler,
	display: Display,
	player: Player,
}

impl Engine {
	pub fn start(
		name: &'static str,
		map_generator: &dyn MapGenerator,
		hexture_dimension: HextureDimension,
		floor_size: u8,
		draw_range: u8,
		key_bindings: Vec<KeyBinding>,
	) -> GameResult<()> {
		
		
		let (mut context, event_loop) = {
			use ggez::conf::*;
			
			let resource_dir = if let Ok(manifest_dir) = std::env::var("CARGO_MANIFEST_DIR") {
				let mut path = std::path::PathBuf::from(manifest_dir);
				path.push("static");
				path
			} else {
				std::path::PathBuf::from("./static")
			};
			
			
			ggez::ContextBuilder::new(name, name)
				.window_setup(WindowSetup::default().title(name))
				.window_mode(WindowMode::default().dimensions(1920.0, 1080.0).resizable(true))
				.add_resource_path(resource_dir)
				.build()?
		};
		
		
		
		let mut entities = EntityStore::new();
		
		
		let center = Position::new(0, 0);
		let mut area = HexagonalArea::new(center, floor_size).spiral_iter();
		
		let player = map_generator.start_tile(center, &mut entities);
		area.next();
		
		for position in area {
			map_generator.tile(position, &mut entities);
		}
		
		
		{//render hextures
			//TODO Somehow move this rendering after Hexture insertion into the ECS
			let mut entities_without_mesh =
				entities.with::<Hexture>()
					.iter()
					.map(|(id, hexture)| (*id, (*hexture).clone()))
					.filter(|(id, _)| entities.get::<graphics::Mesh>(*id).is_none())
					.collect::<Vec<(EntityId, Hexture)>>();
			
			for (id, hexture) in entities_without_mesh.drain(..) {
				let mesh = hexture.render(hexture_dimension.trixel_size(), &mut context);
				entities.put(id, mesh);
			}
		}
		
		
		
		
		let input_handler = InputHandler::new(player, key_bindings);
		
		let display = {
			let player_position = entities.get::<Position>(player.id).cloned().expect("No player position during initialization.");
			
			Display::new(
				hexture_dimension,
				player_position,
				draw_range,
			)
		};
		
		
		let state = Self {
			entities,
			input_handler,
			display,
			player,
		};
		
		
		ggez::event::run(context, event_loop, state)
	}
}

impl ggez::event::EventHandler<ggez::GameError> for Engine {
	
	fn key_down_event(&mut self, context: &mut Context, _: KeyCode, _: KeyMods, _: bool) {
		
		self.input_handler.process(&mut self.entities, context, &mut self.display);
		
		let player_position = self.entities.get::<Position>(self.player.id).cloned().expect("No player position.");
		self.display.set_center(player_position);
	}
	
	
	fn draw(&mut self, context: &mut Context) -> GameResult {
		
		graphics::clear(context, Color::BLACK);
		
		self.display.redraw_tiles(&self.entities, context)?;
		graphics::present(context)?;
		
		Ok(())
	}
	
	
	fn resize_event(&mut self, context: &mut Context, width: f32, height: f32) {
		graphics::set_screen_coordinates(context, graphics::Rect::new(0.0, 0.0, width, height)).expect("Failed to set screen coordinates.");
	}
	
	
	fn update(&mut self, context: &mut Context) -> GameResult {
		
		while ggez::timer::check_update_time(context, 3) {
			entities_take_turn(&mut self.entities);
		}
		
		Ok(())
	}
}



pub fn entities_take_turn(entities: &mut EntityStore) {
	
	let mut entities_with_behavior =
		entities.with::<Behavior>()
			.iter()
			.map(|(id, behavior)| (*id, (*behavior).clone()))
			.collect::<Vec<(EntityId, Behavior)>>();
	
	for (id, behavior) in entities_with_behavior.drain(..) {
		let take_turn = behavior.take_turn;
		take_turn(id, entities);
	}
}
