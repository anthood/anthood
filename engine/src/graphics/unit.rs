use crate::data::Position as HexPosition;
use crate::graphics::HextureDimension;


pub type Pixel = f32;

pub struct ScreenSpace;
pub type PixelPoint = euclid::Point2D<Pixel, ScreenSpace>;
pub type PixelVector = euclid::Vector2D<Pixel, ScreenSpace>;


pub trait UnitMintCompat {
	fn into_mint(self) -> ggez::mint::Point2<Pixel>;
}
impl UnitMintCompat for PixelPoint {
	fn into_mint(self) -> ggez::mint::Point2<Pixel> {
		ggez::mint::Point2 { x: self.x, y: self.y }
	}
}



pub trait FromPosition {
	fn from_position(position: HexPosition, hexture_dimension: HextureDimension) -> Self;
}
impl FromPosition for PixelPoint {
	fn from_position(position: HexPosition, hexture_dimension: HextureDimension) -> Self {
		
		let col = f32::from(position.col);
		let row = f32::from(position.row);
		
		PixelPoint::new(
			hexture_dimension.spacing().x * col,
			hexture_dimension.spacing().y * (row + col/2.0),
		)
	}
}
