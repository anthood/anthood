pub use ggez::graphics::Color;

pub trait Colors {
	const TRANSPARENT: Color;
	const BLACK: Color;
	const WHITE: Color;
	const RED: Color;
	const BLUE: Color;
	const GREEN: Color;
}

impl Colors for Color {
	const TRANSPARENT: Color = Color { r: 0.0, g: 0.0, b: 0.0, a: 0.0 };
	const BLACK: Color	   = ggez::graphics::Color::BLACK;
	const WHITE: Color	   = ggez::graphics::Color::WHITE;
	const RED: Color		 = Color { r: 1.0, g: 0.0, b: 0.0, a: 1.0 };
	const GREEN: Color	   = Color { r: 0.0, g: 1.0, b: 0.0, a: 1.0 };
	const BLUE: Color		= Color { r: 0.0, g: 0.0, b: 1.0, a: 1.0 };
}
