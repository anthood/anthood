use crate::graphics::unit::*;


#[derive(Clone, Copy, Debug)]
pub struct Triangle {
	pub a: PixelVector,
	pub b: PixelVector,
	pub c: PixelVector,
}

impl Triangle {
	pub fn new(a: PixelVector, b: PixelVector, c: PixelVector) -> Self {
		Self { a, b, c }
	}
	
	pub fn flip_vertically(self) -> Self {
		Self {
			a: PixelVector::new(self.a.x, self.b.y),
			b: PixelVector::new(self.b.x, self.a.y),
			c: PixelVector::new(self.c.x, self.b.y),
		}
	}
	
	pub fn translate(self, vector: PixelVector) -> Self {
		Self {
			a: vector + self.a,
			b: vector + self.b,
			c: vector + self.c,
		}
	}
}
