use ggez::Context;
use ggez::graphics::{Mesh, MeshBuilder, Vertex};

use crate::{
	data::{Hexture, trixel},
	graphics::color::*,
	graphics::unit::*,
};

use super::triangle::Triangle;


pub(crate) fn hexture(hexture: &Hexture, trixel_size: PixelVector, context: &mut Context) -> Mesh {
	
	let mut mesh_builder = MeshBuilder::new();
	
	for (trixel_position, color) in hexture.trixels.iter() {
		self::trixel(trixel_position, trixel_size, *color, &mut mesh_builder);
	}
	
	mesh_builder.build(context).expect("Failed to construct Mesh.")
}


fn trixel(trixel_position: &trixel::Position, trixel_size: PixelVector, color: Color, mesh_builder: &mut MeshBuilder) {
	
	let trixel = Triangle::new(
		PixelVector::new(-trixel_size.x / 2.0,  trixel_size.y / 2.0),
		PixelVector::new(				 0.0, -trixel_size.y / 2.0),
		PixelVector::new( trixel_size.x / 2.0,  trixel_size.y / 2.0),
	);
	
	let trixel = trixel
		.translate(trixel_position.pixel_within_hexture(trixel_size).to_vector());
	
	let trixel = if trixel_position.points_north() {
		trixel
	} else {
		trixel.flip_vertically()
	};
	
	
	fn vertex(position: PixelVector, color: Color) -> Vertex {
		Vertex {
			pos: [position.x, position.y],
			uv: [0.0, 0.0],
			color: color.into(),
		}
	}
	
	let triangle_verts = vec![
		vertex(trixel.a, color),
		vertex(trixel.b, color),
		vertex(trixel.c, color),
	];
	
	let triangle_indices = vec![0, 1, 2];
	
	mesh_builder.raw(&triangle_verts, &triangle_indices, None).expect("Failed to add to MeshBuilder.");
}
