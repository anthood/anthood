use std::cmp::Ordering;

use ggez::Context;
use ggez::error::GameResult;
use ggez::graphics;
use ggez::graphics::{Mesh, DrawParam};

use crate::{
	data::*,
	entity::EntityStore,
	graphics::{HextureDimension, color::*, unit::*},
};


pub struct Display {
	hexture_dimension: HextureDimension,
	scale: f32,
	center: Position,
	draw_range: u8,
	fullscreen: bool,
}


impl Display {

	pub fn new(
		hexture_dimension: HextureDimension,
		center: Position,
		draw_range: u8,
	) -> Self {
		Self {
			hexture_dimension,
			scale: 1.0,
			center,
			draw_range,
			fullscreen: false,
		}
	}
	
	
	
	pub fn redraw_tiles(&mut self, entities: &EntityStore, context: &mut Context) -> GameResult {
		
		let area = HexagonalArea::new(self.center, self.draw_range).spiral_iter();
		
		let screen_size = graphics::screen_coordinates(context);
		let half_screen_size = PixelVector::new(screen_size.w / 2.0, screen_size.h / 2.0);
		
		for position in area {
			self.redraw_tile(&position, &entities, half_screen_size, context)?;
		}
		
		Ok(())
	}
	
	
	fn redraw_tile(&mut self, tile: &Position, entities: &EntityStore, half_screen_size: PixelVector, context: &mut Context) -> GameResult {
		
		let mut ids =
			entities.at(tile)
				.iter()
				.map(|id| (*id, entities.get::<Height>(*id).unwrap_or(&Height(0.0))))
				.collect::<Vec<_>>();
		
		ids.sort_by(|(_, Height(h1)), (_, Height(h2))|
			h1.partial_cmp(&h2).unwrap_or(Ordering::Equal)
		);
		
		
		for (id, _) in ids.drain(..) {
			
			if let Some(mesh) = entities.get::<Mesh>(id) {
				
				let entity_pixel_position = {
					let vector_to_player = self.center.to(*tile).to_vector();
					let pixel_position = PixelPoint::from_position(vector_to_player.to_position(), self.hexture_dimension);
					
					pixel_position + half_screen_size
				};
				
				
				
				{ //draw entity texture
					let draw_params = DrawParam::new()
						.scale(ggez::mint::Vector2 { x: self.scale, y: self.scale });
					
					let draw_params = draw_params
						.dest(entity_pixel_position.into_mint());
					
					let direction = entities.get::<Direction>(id).unwrap_or(&Direction::North);
					let draw_params = draw_params
						.rotation(direction.angle_radian());
					
					graphics::draw(context, mesh, draw_params)?;
				}
				
				
				
				if let Some(health) = entities.get::<Health>(id) {
					if health.current_health != health.max_health {
						
						let hexture_edge_length = self.hexture_dimension.hexture_edge_length();
						let trixel_size = self.hexture_dimension.trixel_size();
						
						
						let width = hexture_edge_length * f32::from(health.current_health) / f32::from(health.max_health);
						let height = trixel_size.y;
						
						let health_bar = graphics::Mesh::new_rectangle(
							context,
							graphics::DrawMode::fill(),
							graphics::Rect::new(0.0, 0.0, width, height),
							Color::GREEN
						)?;
						
						
						let top_left = entity_pixel_position - PixelPoint::new(hexture_edge_length / 2.0, self.hexture_dimension.size().y / 2.0);
						let top_left = top_left.to_point();
						
						let draw_params = DrawParam::default()
							.dest(top_left.into_mint());
						
						graphics::draw(context, &health_bar, draw_params)?;
					}
				}
			}
		}
		
		Ok(())
	}
	
	
	pub fn change_zoom_by(&mut self, factor: f32) {
		self.scale *= factor;
		self.hexture_dimension.change_edge_length(factor);
	}
	
	
	pub fn set_center(&mut self, center: Position) {
		self.center = center;
	}
	
	
	pub fn set_fullscreen(&mut self, fullscreen: bool, context: &mut Context) {
		if fullscreen {
			graphics::set_fullscreen(context, ggez::conf::FullscreenType::True).expect("Failed to set to fullscreen.");
		}
		else {
			graphics::set_fullscreen(context, ggez::conf::FullscreenType::Windowed).expect("Failed to set to windowed.");
		}
	}
	
	pub fn toggle_fullscreen(&mut self, context: &mut Context) {
		self.fullscreen = !self.fullscreen;
		
		self.set_fullscreen(self.fullscreen, context);
	}
}
