use super::*;


#[derive(Debug)]
pub struct AttackTowards {
	entity: EntityId,
	direction: Direction,
}

impl AttackTowards {
	pub fn new(entity: EntityId, direction: Direction) -> Self {
		Self { entity, direction }
	}
}

impl Action for AttackTowards {
	fn apply(&self, entities: &mut EntityStore) -> ActionResult {
		
		if let Some(entity_direction) = entities.get::<Direction>(self.entity).cloned() {
			if entity_direction != self.direction {
				return Ok(())
			}
		}
		
		if let Some(position) = entities.get::<Position>(self.entity).cloned() {
			let new_pos = position + self.direction.normalized_vector();
			
			let entities_with_health =
				entities.at(&new_pos)
				.iter()
				.filter(|entity| entities.get::<Health>(**entity).is_some())
				.cloned()
				.collect::<Vec<_>>();
			
			
			if !entities_with_health.is_empty() {
				entities_with_health.iter().for_each(|entity| {
					let health = entities.get::<Health>(*entity).cloned().unwrap();
					
					if health.current_health == 1 {
						entities.delete_entity(*entity);
					}
					else {
						entities.put(*entity, Health { current_health: health.current_health - 1, ..health })
					}
				});
			}
			
			return Ok(());
		}
		
		Err(())
	}
}
