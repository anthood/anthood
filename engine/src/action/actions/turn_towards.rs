use super::*;


#[derive(Debug)]
pub struct TurnTowards {
	entity: EntityId,
	direction: Direction,
}

impl TurnTowards {
	pub fn new(entity: EntityId, direction: Direction) -> Self {
		Self { entity, direction }
	}
}

impl Action for TurnTowards {
	fn apply(&self, entities: &mut EntityStore) -> ActionResult {
		let target_direction = self.direction;
		
		if let Some(current_direction) = entities.get::<Direction>(self.entity) {
			
			let new_view_direction =
				if target_direction == current_direction.opposite() {
					*current_direction //no automatic handling for turning around
				}
				else if target_direction == current_direction.neighbor_cw().neighbor_cw() {
					current_direction.neighbor_cw() //turn halfway when turning backward-right
				}
				else if target_direction == current_direction.neighbor_ccw().neighbor_ccw() {
					current_direction.neighbor_ccw() //turn halfway when turning backward-left
				}
				else {
					target_direction //forward, forward-left and forward-right work normally
				};
				
				
			if *current_direction != new_view_direction {
				entities.put(self.entity, new_view_direction);
			}
			
			return Ok(());
		}
		
		Err(())
	}
}
