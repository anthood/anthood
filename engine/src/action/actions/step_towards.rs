use super::*;


#[derive(Debug)]
pub struct StepTowards {
	entity: EntityId,
	direction: Direction,
}

impl StepTowards {
	pub fn new(entity: EntityId, direction: Direction) -> Self {
		StepTowards { entity, direction }
	}
}

impl Action for StepTowards {
	fn apply(&self, entities: &mut EntityStore) -> ActionResult {
		
		if entities.get::<Direction>(self.entity).is_some() {
			TurnTowards::new(self.entity, self.direction).apply(entities)?; //turning can be done while taking a step
		}
		
		
		let position = entities.get::<Position>(self.entity);
		
		if let Some(position) = position.cloned() {
			let new_pos = position + self.direction.normalized_vector();
			
			let self_impassable = entities.get::<Impassable>(self.entity).is_some();
			
			let new_pos_is_impassable =
				entities
					.at(&new_pos)
					.iter()
					.any(|id| entities.get::<Impassable>(*id).is_some());
			
			
			if self_impassable && new_pos_is_impassable {
				AttackTowards::new(self.entity, self.direction).apply(entities)?;
			}
			else {
				entities.put(self.entity, new_pos);
			}
			
			return Ok(());
		}
		
		Err(())
	}
}
