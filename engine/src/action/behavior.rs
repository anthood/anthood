
use crate::entity::{EntityId, EntityStore};


#[derive(Clone)]
pub struct Behavior {
	pub take_turn: fn(EntityId, &mut EntityStore),
}

impl Behavior {
	pub fn new(take_turn: fn(EntityId, &mut EntityStore)) -> Self {
		Self {
			take_turn,
		}
	}
}

