use crate::entity::EntityStore;


pub type ActionResult = Result<(), ()>;

pub trait Action: std::fmt::Debug {
	fn apply(&self, entities: &mut EntityStore) -> ActionResult;
}
