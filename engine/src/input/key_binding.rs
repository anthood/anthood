use std::collections::HashSet;

use ggez::input::keyboard::KeyCode;

use super::input_action::InputAction;

#[derive(Debug)]
pub struct KeyBinding {
	pub action: Box<dyn InputAction>,
	pub keys: HashSet<KeyCode>,
}

impl PartialEq for KeyBinding {
	fn eq(&self, other: &Self) -> bool {
		self.keys.eq(&other.keys)
	}
}

impl Eq for KeyBinding {}



#[macro_export]
macro_rules! bind {
	($action:expr => $($key:expr),+) => {{
		let mut keys: HashSet<KeyCode> = HashSet::new();
		$(
			keys.insert($key);
		)*
		
		KeyBinding { action: Box::new($action), keys }
	}};
}


#[cfg(test)]
mod tests {
	use super::*;
	use super::super::input_action::*;
	use KeyCode::*;
	
	
	#[test]
	fn bind_macro() {
		use std::iter::FromIterator;
		use crate::data::Direction::*;
		
		assert_eq!(
			bind![Move(North) => LControl, W],
			KeyBinding {action: Box::new(Move(North)), keys: HashSet::from_iter(vec![LControl, W])}
		);
	}
}
