use ggez::Context;

use crate::{
	action::*,
	data::{Player, Direction},
	graphics::Display,
	entity::EntityStore,
};

use super::StateUpdate;


pub trait InputAction: std::fmt::Debug {
	fn name(&self) -> String;
	fn execute(&self, player: Player, entities: &mut EntityStore, context: &mut Context, display: &mut Display) -> StateUpdate;
}



#[derive(Debug)]
pub struct Move(pub Direction);
impl InputAction for Move {
	fn name(&self) -> String {
		format!("Move {}", self.0)
	}
	
	fn execute(&self, player: Player, entities: &mut EntityStore, _context: &mut Context, _display: &mut Display) -> StateUpdate {
		match StepTowards::new(player.id, self.0).apply(entities) {
			Ok(_) => StateUpdate::TurnPassed,
			Err(_) => StateUpdate::Noop,
		}
	}
}



#[derive(Debug)]
pub struct Turn(pub Direction);
impl InputAction for Turn {
	fn name(&self) -> String {
		format!("Move {}", self.0)
	}
	
	fn execute(&self, player: Player, entities: &mut EntityStore, _context: &mut Context, _display: &mut Display) -> StateUpdate {
		match TurnTowards::new(player.id, self.0).apply(entities) {
			Ok(_) => StateUpdate::TurnPassed,
			Err(_) => StateUpdate::Noop,
		}
	}
}


#[derive(Debug)]
pub struct ZoomIn;
impl InputAction for ZoomIn {
	fn name(&self) -> String {
		"Zoom In".to_string()
	}
	
	fn execute(&self, _player: Player, _entities: &mut EntityStore, _context: &mut Context, display: &mut Display) -> StateUpdate {
		display.change_zoom_by(1.1);
		
		StateUpdate::Redraw
	}
}

#[derive(Debug)]
pub struct ZoomOut;
impl InputAction for ZoomOut {
	fn name(&self) -> String {
		"Zoom Out".to_string()
	}
	
	fn execute(&self, _player: Player, _entities: &mut EntityStore, _context: &mut Context, display: &mut Display) -> StateUpdate {
		display.change_zoom_by(0.9);
		
		StateUpdate::Redraw
	}
}


#[derive(Debug)]
pub struct ToggleFullscreen;
impl InputAction for ToggleFullscreen {
	fn name(&self) -> String {
		"Toggle Fullscreen".to_string()
	}
	
	fn execute(&self, _player: Player, _entities: &mut EntityStore, context: &mut Context, display: &mut Display) -> StateUpdate {
		display.toggle_fullscreen(context);
		
		StateUpdate::Noop
	}
}
