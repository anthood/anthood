mod input_handler;
pub use input_handler::{InputHandler, StateUpdate};

pub mod input_action;

#[macro_use]
pub mod key_binding;

pub use ggez::input::keyboard;
