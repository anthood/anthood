use ggez::Context;
use ggez::input::keyboard;

use crate::{
	data::Player,
	graphics::Display,
	entity::EntityStore,
};

use super::key_binding::KeyBinding;


#[derive(PartialEq, Debug)]
pub enum StateUpdate {
	TurnPassed,
	Redraw,
	Noop,
}


pub struct InputHandler {
	player: Player,
	keybindings: Vec<KeyBinding>,
}

impl InputHandler {
	
	pub fn new(player: Player, mut keybindings: Vec<KeyBinding>) -> Self {
		
		//Ensure the keybindings are in order from most simultaneously pressed keys to least,
		//to prevent keybindings from being triggered that are a subset of the pressed keys.
		keybindings.sort_by(|a, b| a.keys.len().partial_cmp(&b.keys.len()).unwrap());
		keybindings.reverse();
		
		
		Self {
			player,
			keybindings,
		}
	}
	
	pub fn process(&mut self, entities: &mut EntityStore, context: &mut Context, display: &mut Display) -> StateUpdate {
		for keybinding in self.keybindings.iter() {
			if &keybinding.keys == keyboard::pressed_keys(context) {
				return keybinding.action.execute(self.player, entities, context, display);
			}
		}
		
		StateUpdate::Noop
	}
}
