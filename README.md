# ![Anthood](static/logo.png)

A simulation for ants.

![](static/screenshots/closeup.png)


## Compiling

### Native
With `cargo` and Rust Stable set up on your system, run the following command in the source code directory:

```
cargo run --release
```

### Web
To compile for web, first install cargo-web like so:

```
cargo install cargo-web
```

Then run the following command in the source code directory:

```
cargo web start --release --open
```

## Controls
Movement is done with [QWEASD].  
Additionally hold [Shift] to turn on the current position.

The zoom can be changed via [+] and [-].  
Fullscreen can be toggled with [F11].
